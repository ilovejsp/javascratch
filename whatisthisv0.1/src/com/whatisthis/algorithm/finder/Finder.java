package com.whatisthis.algorithm.finder;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class Finder {
	
	
	public static String Search(String key) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(
				"http://search.zum.com/search.zum?method=web&option=accu&query="+key);
				StringBuilder content = new StringBuilder();

		try {
			HttpResponse response = httpClient.execute(httpGet);

			int bufferLength = 1024;
			byte[] buffer = new byte[bufferLength];
			InputStream is = response.getEntity().getContent();

			while (is.read(buffer) != -1) {
				content.append(new String(buffer, "UTF-8"));
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}
	
	public static ArrayList<String[]> GetResultArrayList(String arr[]){
		ArrayList<String[]> resultarraylist = new ArrayList<String[]>();
		String key = "";
		
		for(int i=0; i<arr.length ; i++){
			key=arr[i];
			Finder crawler = new Finder();
			int start = 0;
			int end = 0;
			String finalresult = "";
			String result = "";
			result = crawler.Search(key);
			result = result.replaceAll(
					"<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
			start = result.indexOf("</h4>");
			start = start + "</h4>".length();
			end = result.indexOf("더보기"); 
			finalresult = ((String) result.subSequence(start, end))
					.replace(".", "");
			String resultarr[]=finalresult.split(" ");
			resultarraylist.add(resultarr);
		}
		
		return resultarraylist;
	}
	
	public static String[] MainAlgorithm(String arr[]){

		String returnarr[] = new String[100];
		int returnarrindex=0;
		ArrayList<String[]> list = new ArrayList<String[]>();
		list=Finder.GetResultArrayList(arr);
		
		
		for(int i=0; i<list.size(); i++){
			for(int j=0; j<list.get(i).length; j++){
				returnarr[returnarrindex]=list.get(i)[j];
				returnarrindex++;
			}
		}

		return returnarr;
	}

}
